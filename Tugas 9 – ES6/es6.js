
  //Tugas 9 es6.js dibuat oleh Nafiah Rahma
// Soal No. 1. Mengubah fungsi menjadi fungsi arrow
console.log('-------------- Soal No 1 -----------------');
console.log("\n")
{
    const golden = goldenFunction = () => console.log("this is golden!!") 
    golden()       
       
}
console.log("\n")
console.log('------------------------------------------');
// Soal No. 2. Sederhanakan menjadi Object literal di ES6
console.log('-------------- Soal No 2 -----------------');
console.log("\n")
{
    const newFunction = literal=(firstName, lastName)=>{
        return {
          firstName,
          lastName,
          fullName: function(){
            console.log(firstName + " " + lastName)
            return 
          }
        }
      }
       
      //Driver Code 
      newFunction("William", "Imoh").fullName() 

}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 3. Destructuring
console.log('-------------- Soal No 3 -----------------');
console.log("\n")
{
    const newObject = {
        firstName: "Harry",
        lastName: "Potter Holt",
        destination: "Hogwarts React Conf",
        occupation: "Deve-wizard Avocado",
        spell: "Vimulus Renderus!!!"
      }
        const{ firstName, lastName, destination, occupation}=newObject;

        // Driver code
        console.log(firstName, lastName, destination, occupation)

}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 4. Array Spreading
console.log('-------------- Soal No 4 -----------------');
console.log("\n")
{
        const west = ["Will", "Chris", "Sam", "Holly"]
        const east = ["Gill", "Brian", "Noel", "Maggie"]
        
        const combined = [...west, ...east]
        //Driver Code
        console.log(combined) 
}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 5. Template Literals
console.log('-------------- Soal No 5 -----------------');
console.log("\n")

{
        const planet = "earth"
        const view = "glass"
        const before = `Lorem  ${view} dolor sit amet, ` +  
        `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
        'incididunt ut labore et dolore magna aliqua. Ut enim' +
        ' ad minim veniam'
            
        // Driver Code
        console.log(before) 

}
console.log("\n")
console.log('--------------------------------------------');

