//Tugas 5 Array dibuat oleh Nafiah Rahma
// Soal No. 1 (Range)
console.log('-------------- Soal No 1 -----------------');
console.log("\n")
{ 
    function range(startNum, finishNum) {
        var number=[];

        if (startNum>finishNum){
           
            for(var angka = startNum; angka>=finishNum; angka--) {
                number.push(angka)               
            }   
            return  number         
        }
        else if (startNum<finishNum){
            for(var angka = startNum; angka<=finishNum; angka++) {
                number.push(angka)  
            }
            return  number
        } 
        else{
            return -1
        } 
    }

    console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] 
    console.log(range(1)) // -1
    console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
    console.log(range(54, 50)) // [54, 53, 52, 51, 50]
    console.log(range()) // -1 

}
console.log("\n")
console.log('------------------------------------------');
// Soal No. 2
console.log('-------------- Soal No 2 -----------------');
console.log("\n")

{
    function rangeWithStep(startNum, finishNum, step) {
        var number=[];

        if (startNum>finishNum){           
            for(var angka = startNum; angka>=finishNum; angka-=step) {
                 number.push(angka)               
            }            
        }
        else if (startNum<finishNum){
            for(var angka = startNum; angka<=finishNum; angka+=step) {
                number.push(angka)  
            }
        }  
        return number
    }
    console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
    console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
    console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
    console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 3
console.log('-------------- Soal No 3 -----------------');
console.log("\n")
{
    function sum(startNum, finishNum, step) {
        var number=[]  
        var jumlah=null      
        if(startNum==null && finishNum==null && step==null){
            return 0
        }
        else if( finishNum==null && step==null){
            return startNum
        }       
        
        else if(step==null){
            if (startNum>finishNum){           
                for(var angka = startNum; angka>=finishNum; angka--) {
                    number.push(angka)              
                }            
            }
            else if (startNum<finishNum){
                for(var angka = startNum; angka<=finishNum; angka++) {
                    number.push(angka)
                }
            }
            for(var i= 0; i<number.length; i++) {        
                jumlah = jumlah + number[i]
            }
            return jumlah
        }
        else{
            if (startNum>finishNum){           
                for(var angka = startNum; angka>=finishNum; angka-=step) {
                    number.push(angka)              
                }            
            }
            else if (startNum<finishNum){
                for(var angka = startNum; angka<=finishNum; angka+=step) {
                    number.push(angka)
                }
            } 
            for(var i= 0; i<number.length; i++) {        
                jumlah = jumlah + number[i]
            }
            return jumlah 
        }
       
        
    }

    console.log(sum(1,10)) // 55
    console.log(sum(5, 50, 2)) // 621
    console.log(sum(15,10)) // 75
    console.log(sum(20, 10, 2)) // 90
    console.log(sum(1)) // 1
    console.log(sum()) // 0

}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 4
console.log('-------------- Soal No 4 -----------------');
console.log("\n")
{
    function dataHandling(input) {
        var gabung=input
        
        for(var i = 0; i < gabung.length; i++){
            console.log('Nomor ID: ' + gabung[i][0]);
            console.log('Nama Lengkap:' + gabung[i][1]);
            console.log('TTL: ' + gabung[i][2]);
            console.log('Hobi ' + gabung[i][3]);        
            console.log(' ');
        }    
    }
    
    
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] 
    
    dataHandling(input);
   /*  Nomor ID:  0001
    Nama Lengkap:  Roman Alamsyah
    TTL:  Bandar Lampung 21/05/1989
    Hobi:  Membaca
    
    Nomor ID:  0002
    Nama Lengkap:  Dika Sembiring
    TTL:  Medan 10/10/1992
    Hobi:  Bermain Gitar
    
    Nomor ID:  0003
    Nama Lengkap:  Winona
    TTL:  Ambon 25/12/1965
    Hobi:  Memasak
    
    Nomor ID:  0004
    Nama Lengkap:  Bintang Senjaya
    TTL:  Martapura 6/4/1970
    Hobi:  Berkebun  */

}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 5
console.log('-------------- Soal No 5 -----------------');
console.log("\n")
{

   function balikKata(kalimat){
       var a=kalimat
       var panjang= a.length-1
       var balik=""

       for (i=panjang;i>=0;i--)
       {
        balik=balik+a.charAt(i)
       }
       return balik
    }
    
    console.log(balikKata("Kasur Rusak")) // kasuR rusaK
    console.log(balikKata("SanberCode")) // edoCrebnaS
    console.log(balikKata("Haji Ijah")) // hajI ijaH
    console.log(balikKata("racecar")) // racecar
    console.log(balikKata("I am Sanbers")) // srebnaS ma I 
 
}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 6
console.log('-------------- Soal No 6 -----------------');
console.log("\n")
{
    function dataHandling2(input) {
        var array=input
        var ttl,ttlpisah,tanggal ,bulan, tahun, namabulan, ttlpisahsorting, ttlpisahjoin, nama, namairisan;
        //awal : ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
        //akhir: ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
        
        array.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
        array.splice(4, 1,  "Pria", "SMA Internasional Metro")
        console.log(array);
        //bulan 
        ttl= array[3]
        ttlpisah = ttl.split("/")
        tanggal=ttlpisah[0]
        bulan=ttlpisah[1]
        tahun=ttlpisah[2]
        //pengambilan nama bulan
                switch(bulan) {
                    case "01":   { namabulan="Januari"; break; }
                    case "02":   { namabulan="Februari" ; break; }
                    case "03":   { namabulan="Maret "; break; }
                    case "05":   { namabulan="Mei" ; break; }
                    case "06":   { namabulan="Juni" ; break; }
                    case "07":   { namabulan="Juli" ; break; }
                    case "08":   { namabulan="Agustus"; break; }
                    case "09":   { namabulan="September"; break; }
                    case "10":   { namabulan="Oktober" ; break; }
                    case "11":   { namabulan="November" ; break; }
                    case "12":   { namabulan="Desember"; break; }
                } 
        console.log(namabulan);
        //sorting ttl    
        ttlpisahsorting=[tanggal,bulan,tahun]
        ttlpisahsorting.sort(function(a, b){return b-a});
        console.log(ttlpisahsorting)
    
        //penggunaan join
        ttlpisahjoin = ttlpisah.join("-")
        console.log(ttlpisahjoin) 
    
        //penggunaan slice
        nama=array[1]
        namairisan = nama.slice(0,15) 
        console.log(namairisan)         
    
    }
    
    
    
    var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
    dataHandling2(input)
    /**
     * keluaran yang diharapkan (pada console)
     *
     * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
     * Mei
     * ["1989", "21", "05"]
     * 21-05-1989
     * Roman Alamsyah
     */ 
    

}
console.log("\n")
console.log('--------------------------------------------');
