//Tugas 6 object js dibuat oleh Nafiah Rahma
// Soal No. 1 (Array to Object)
console.log('-----------Soal No. 1 (Array to Object)------------');
console.log("\n")
{
    function arrayToObject(arr) {
        
        var people=arr
        for(var i = 0; i < people.length; i++){
            var urut=i+1
            console.log(urut+". "+ people[i][0]+" "+ people[i][1]+" : ") 
            var umur1=people[i][3]
            var now = new Date()
            var thisYear = now.getFullYear() // 2020 (tahun sekarang)
            var umu2=""
            if(umur1==null || umur1>thisYear){
                umur2="Invalid Birth Year"
            }
            else{
                umur2=thisYear-umur1
            }

            var orang = {    
                firstName: people[i][0],
                lastName: people[i][1],
                gender: people[i][2],
                age: umur2
            }
            console.log(orang)
        }
    }
     
    // Driver Code
    var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
    arrayToObject(people) 
    /*
        1. Bruce Banner: { 
            peopleName: "Bruce",
            lastName: "Banner",
            gender: "male",
            age: 45
        }
        2. Natasha Romanoff: { 
            peopleName: "Natasha",
            lastName: "Romanoff",
            gender: "female".
            age: "Invalid Birth Year"
        }
    */
     
    var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
    arrayToObject(people2) 
    /*
        1. Tony Stark: { 
            peopleName: "Tony",
            lastName: "Stark",
            gender: "male",
            age: 40
        }
        2. Pepper Pots: { 
            peopleName: "Pepper",
            lastName: "Pots",
            gender: "female".
            age: "Invalid Birth Year"
        }
    */
     
    // Error case  arrayToObject([]) // ""


}
console.log("\n")
console.log('------------------------------------------------------');
// Soal No. 2 (Shopping Time)
console.log('--------------Soal No. 2 (Shopping Time)--------------');
console.log("\n")
{
    function shoppingTime(memberId, money) {
        var Id=memberId
        var uang=money        
          
    
        if( ( Id=='') || (Id==null && uang==null ) ){
            return "Mohon maaf, toko X hanya berlaku untuk member saja"
        } 
        else if(Id!='' && uang<50000){
            return "Mohon maaf, uang tidak cukup"
        }else if(Id!='' && uang>50000) {
        
            var sisa=uang   
            var belanja=null     
            var listbelanja=[] 
            for(var i = sisa; i > 0; i--){
                var cek1=listbelanja.includes("Sepatu Stacattu")
                var cek2=listbelanja.includes("Baju Zoro")
                var cek3=listbelanja.includes("Baju H&N")
                var cek4=listbelanja.includes("Sweater Uniklooh")
                var cek5=listbelanja.includes("Casing Handphone")
    
                if(sisa>=1500000 && cek1==false ){
                    listbelanja.push( "Sepatu Stacattu")
                    sisa=sisa-1500000
                }else if(sisa>=500000 && cek2==false ){
                    listbelanja.push( "Baju Zoro")
                    sisa=sisa-500000
                }else if(sisa>=250000 && cek3==false ){
                    listbelanja.push( "Baju H&N")
                    sisa=sisa-250000
                }else if(sisa>=175000 && cek4==false ){
                    listbelanja.push( "Sweater Uniklooh")
                    sisa=sisa-175000
                }else if(sisa>=50000 && cek5==false  ){
                    listbelanja.push( "Casing Handphone")
                    sisa=sisa-50000
                }
            }           
    
            belanja= {    
                memberId: Id,
                money: uang,
                listPurchased: listbelanja,
                changeMoney: sisa }
            }
            return belanja
        
        
      }
       
      // TEST CASES
      console.log(shoppingTime('1820RzKrnWn08', 2475000));
        //{ memberId: '1820RzKrnWn08',
        // money: 2475000,
        // listPurchased:
        //  [ 'Sepatu Stacattu',
        //    'Baju Zoro',
        //    'Baju H&N',
        //    'Sweater Uniklooh',
        //    'Casing Handphone' ],
        // changeMoney: 0 }
     console.log(shoppingTime('82Ku8Ma742', 170000))
      //{ memberId: '82Ku8Ma742',
      // money: 170000,
      // listPurchased:
      //  [ 'Casing Handphone' ],
      // changeMoney: 120000 }
      console.log(shoppingTime('', 2475000)) //Mohon maaf, toko X hanya berlaku untuk member saja
      console.log(shoppingTime('234JdhweRxa53', 15000));//Mohon maaf, uang tidak cukup
      console.log(shoppingTime()) ////Mohon maaf, toko X hanya berlaku untuk member saja 

}
console.log("\n")
console.log('-------------------------------------------------------');
//Soal No. 3 (Naik Angkot)
console.log('---------------Soal No. 3 (Naik Angkot)---------------');
console.log("\n")
{
    function naikAngkot(arrPenumpang) {
        rute = ['A', 'B', 'C', 'D', 'E', 'F'];
        var people=arrPenumpang
        var balik=[]        
        
        for(var i = 0; i <people.length; i++){              
            var awal=rute.indexOf(people[i][1])+1;
            var akhir=rute.indexOf(people[i][2])+1;  
            var uang=(akhir-awal)*2000
            var arah = {    
                penumpang: people[i][0],
                naikDari: people[i][1],
                tujuan: people[i][2],
                bayar: uang
            }  
            balik.push(arah)
        }
        return balik     
    }
       
      //TEST CASE
      console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
      // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
      //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
       
      console.log(naikAngkot([])); //[]

}
console.log("\n")
console.log('-------------------------------------------------------');
  
