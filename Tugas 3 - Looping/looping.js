//Tugas 3 Looping dibuat oleh Nafiah Rahma  
console.log('-------------------------------');
console.log("Soal No 1")
console.log("\n")
// Soal No. 1 Looping While

{
    var deret = 2;
    var jumlah = 2;

    console.log('LOOPING PERTAMA') // judul
    while(deret < 21) { // Loop akan terus berjalan selama nilai deret masih di dibawah 21
        console.log(deret+' - I love coding ')
        deret += jumlah; // Menambahkan nilai variable deret dengan angka jumlah
    }
    
    deret -= jumlah; //agar nilai variabel deret menjadi 20
    console.log('LOOPING KEDUA ' + jumlah)// judul
    while(deret >1) { // Loop akan terus berjalan selama nilai deret masih di atas 1
        console.log(deret+' - I will become a mobile developer ')
        deret -= jumlah; // Menambahkan nilai variable deret dengan angka jumlah
    }
}

console.log("\n")
console.log('-------------------------------');
console.log("Soal No 2")
console.log("\n")
// Soal No. 2 Looping menggunakan for
{
    console.log("OUTPUT")
    for(var angka = 1; angka < 21; angka++) {  // loop akan berjalan selama nilai angka dibawah 21
        if (angka%2==0) {    // angka habis dibagi 2 maka akan menghasilkan bilangan genap
            console.log( angka +' - Berkualitas' );             
        } else if(angka%3==0) {  // angka habis dibagi 3 maka kelipatan 3
            console.log( angka +' - I Love Coding' );
        } else{  // sisanya bilangan ganjil yang bukan kelipatan tigaa
            console.log( angka +' - Santai' );
        }  
    } 
}

console.log("\n")
console.log('-------------------------------');
console.log("Soal No 3")
console.log("\n")
// Soal No. 3 Membuat Persegi Panjang
{
    var persegipanjang=""
    for(var baris = 1; baris < 5; baris++) {
        for(var kelipatan = 1; kelipatan <9; kelipatan++) {
            persegipanjang+='#'
          }
          persegipanjang+='\n'
      } 
      console.log(persegipanjang);           
}
console.log("\n")
console.log('-------------------------------');
console.log("Soal No 4")
console.log("\n")

// Soal No. 4 Membuat Tangga
{
    var tangga="#"
    for(var baris = 1; baris < 8; baris++) {
        console.log(tangga);
        tangga= tangga + '#'
      } 
      

}
console.log("\n")
console.log('-------------------------------');
console.log("Soal No 5")
console.log("\n")

// Soal No. 5 Membuat Papan Catur

{
    for(var baris = 1; baris < 9; baris++) {        
        if (baris%2==0) {  // menampilkan baris genap
            var catur=""              
            for(var kelipatan = 1; kelipatan <9; kelipatan++) { 
                if (kelipatan%2==0) {  //sat kelipatan genap/ papan berwarna putih akan memakai tanda spasi ( )
                    catur+=' '
                } else {
                    catur+='#'     // kelipatan ganjil saat papan berwarna hitam akan memakai tanda pagar (#) 
                }     
            } 
            console.log(catur)
        } else {  // menampilkan baris ganjil
            var catur=""
            for(var kelipatan = 1; kelipatan <9; kelipatan++) { 
                if (kelipatan%2==0) {
                    catur+='#'      // kelipatan ganjil saat papan berwarna hitam akan memakai tanda pagar (#)
                } else {
                    catur+=' '    //sat kelipatan genap/ papan berwarna putih akan memakai tanda spasi ( )
                }     
            }
            console.log(catur)
        }
        
      }  

}
console.log('-------------------------------');

