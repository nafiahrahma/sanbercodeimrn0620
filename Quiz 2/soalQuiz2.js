// Quiz 2 Oleh Nafiah Rahma
/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

// 1. SOAL CLASS SCORE
console.log('--------------  1. SOAL CLASS SCORE -----------------');
console.log("\n")

class Score {
  constructor(subject, points,email) {
    this.subject = subject
    this.points = points
    this.email = email
  }
  averagee(points) {
    var sum = 0;
    for( var i = 0; i < points.length; i++ ){
    sum += parseInt( points[i], 10 ); 
    }
    var avg = sum/points.length;
    return avg;
  }
}
var siswa= new Score("didi",[1,2,3],"didi.dudu@gmail.com");
console.log(siswa);
console.log(`Rata - rata nilai dari  ${siswa.subject} adalah ${siswa.averagee(siswa.points)}`);
var siswi= new Score("sasa",3,"sasa.sasi@gmail.com");
console.log(siswi); 


console.log("\n")
console.log('--------------------------------------------');

//2. SOAL Create Score
console.log('--------------  2. SOAL Create Score  -----------------');

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  var arraydata=data;
  var subjek=subject;
   
    if(subjek=="quiz-1"){
      console.log(data[0][1])
      var quis1=[]
      for(var i = 1; i <=  (data.length-1); i++){
       //{email: "abduh@mail.com", subject: "quiz-1", points: 78}
        //console.log(data[i][1])
        console.log(`{email: ${data[i][0]} subject: ${data[0][1]}, points:${data[i][1]} }`)
      }

    } else if(subjek=="quiz-2"){
     console.log(data[0][2])
      for(var i = 1; i <=  (data.length-1); i++){
        console.log(`{email: ${data[i][0]} subject: ${data[0][2]}, points:${data[i][2]} }`)
      }

    }else if(subjek=="quiz-3"){
      console.log(data[0][3])
      for(var i = 1; i <=  (data.length-1); i++){
        console.log(`{email: ${data[i][0]} subject: ${data[0][3]}, points:${data[i][3]} }`)
      }

    } 
  

}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

console.log("\n")
console.log('--------------------------------------------');

//3. SOAL Recap Score 
console.log('--------------  3. SOAL Recap Score  -----------------');

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  var people=data
        for(var i = 1; i <=(people.length-1); i++){
            var urut=i            
           
            var rata=0;
            var sum = 0;
            sum = people[i][1]+people[i][2]+people[i][3]
            rata= sum/3;

            var predikat=null
            if(rata>90 && rata<100){
              predikat="honour"
            }
            else if(rata<=90 && rata>80){
            predikat="graduate"
            }
            else if(rata>70 && rata<=80){
            predikat="participant"
            }

            console.log(`${urut}. Email: ${people[i][0]} \n Rata-rata: ${rata} \n Predikat: ${predikat} \n`)         

          }

}

recapScores(data);
