//Tugas 4 function dibuat oleh Nafiah Rahma
// Soal No. 1 
console.log('-------------- Soal No 1 -----------------');
console.log("\n")
{
    function teriak() {
        return "Halo Sanbers!"
    } 
    console.log(teriak()) // "Halo Sanbers!" 

}
console.log("\n")
console.log('------------------------------------------');
// Soal No. 2
console.log('-------------- Soal No 2 -----------------');
console.log("\n")

{ 
    function kalikan(satu, dua){
        return satu * dua
    }
    
    var num1 = 12
    var num2 = 4
    
    var hasilKali = kalikan(num1, num2)
    console.log(hasilKali) // hasilnya 48

}
console.log("\n")
console.log('--------------------------------------------');
// Soal No. 3
console.log('-------------- Soal No 3 -----------------');
console.log("\n")

{
    function introduce(Name,Age,Address, Hobby){
        return "Nama saya " +Name+", umur saya " +Age+" tahun, alamat saya di  " +Address+", dan saya punya hobby yaitu " +Hobby+"!"
    }
    
    var name = "Agus"
    var age = 30
    var address = "Jln. Malioboro, Yogyakarta"
    var hobby = "Gaming"
    
    var perkenalan = introduce(name, age, address, hobby)
    console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"

}
console.log("\n")
console.log('--------------------------------------------');