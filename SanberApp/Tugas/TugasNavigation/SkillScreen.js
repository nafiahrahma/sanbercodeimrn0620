import React, { Component } from 'react'
import { View,
  Image,
   StyleSheet,
  Text,
  ScrollView
  } from 'react-native'

//import Toggle from './Latihan/Toggle'
import IconD from 'react-native-vector-icons/FontAwesome5';

import Icon from 'react-native-vector-icons/Ionicons';
export default class SkillScreen extends Component {

  state = {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'strech',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    return (
      <View style={styles.container}>        
        <View style={[styles.layout, layoutStyle]}>
        <View style={styles.logojudul} >              
                <Image style={styles.logo} source={require('./Tugas/Tugas13/images/logo.png')} />  
                <Text style={styles.logoitem}>PORTOFOLIO</Text>
              
          </View>
          <View style={styles.judul} > 
          <ScrollView horizontal>
              <View style={styles.boxHor1}  >
                  <Icon name="md-person"  size={25} style={styles.ikon1}/>    
              </View>
              <View style={styles.boxHor2} >                
                <Text style={styles.judul1}>Hai</Text>           
                <Text style={styles.judul2}>Rahmawati Nafiah</Text>
              </View>
            </ScrollView>                 
          </View>
          <View style={styles.body}>
          <View style={styles.Box}>
              <Text style={styles.judul3}>SKILL</Text>
              <View style = {styles.lineStyle2} />
          </View>
          <View style={styles.Box1}>
            <ScrollView horizontal >
              <View style={styles.boxSmall}  >
                <Text style={styles.isi} >Library/ Framework</Text>
              </View>
              <View style={styles.boxSmall} >                         
                <Text style={styles.isi} >Bahasa Pemrograman</Text>
              </View>
              <View style={styles.boxSmall} >                         
                <Text style={styles.isi} >Teknologi</Text>
              </View>              
            </ScrollView>                     
            </View>   
                 
            <View style={styles.Box2}>
            <ScrollView horizontal >
              <View style={styles.boxhor21} >                         
                <IconD name="react" size={80}  style={styles.ikon}/>
              </View>
              <View style={styles.boxhor22} >                         
                  <Text style={styles.judul4}>React Native</Text>
                  <Text style={styles.jenis}>Library/Framework</Text>                  
                  <Text style={styles.persen}>50%</Text> 
              </View>
              <View style={styles.boxhor23} >                         
                <IconD name="chevron-right" size={80}  style={styles.ikon2}/>
              </View>            
            </ScrollView> 
            </View>  
            <View style={styles.Box2}>
            <ScrollView horizontal >
              <View style={styles.boxhor21} >                         
                <IconD name="laravel" size={70}  style={styles.ikon}/>
              </View>
              <View style={styles.boxhor22} >                         
                  <Text style={styles.judul4}>Laravel</Text>
                  <Text style={styles.jenis}>Library/Framework</Text>                  
                  <Text style={styles.persen}>100%</Text> 
              </View>
              <View style={styles.boxhor23} >                         
                <IconD name="chevron-right" size={80}  style={styles.ikon2}/>
              </View>            
            </ScrollView> 
            </View>       
            <View style={styles.Box2}>
            <ScrollView horizontal >
              <View style={styles.boxhor21} >                         
                <Icon name="logo-javascript" size={80}  style={styles.ikon}/>
              </View>
              <View style={styles.boxhor22} >                         
                  <Text style={styles.judul4}>Java Script</Text>
                  <Text style={styles.jenis}>bahasa Pemrograman</Text>                  
                  <Text style={styles.persen}>30%</Text> 
              </View>
              <View style={styles.boxhor23} >                         
                <IconD name="chevron-right" size={80}  style={styles.ikon2}/>
              </View>            
            </ScrollView> 
            </View>       
            <View style={styles.Box2}>
            <ScrollView horizontal >
              <View style={styles.boxhor21} >                         
                <IconD name="python" size={80}  style={styles.ikon}/>
              </View>
              <View style={styles.boxhor22} >                         
                  <Text style={styles.judul4}>Pyton</Text>
                  <Text style={styles.jenis}>Bahasa Pemrograman</Text>                  
                  <Text style={styles.persen}>70%</Text> 
              </View>
              <View style={styles.boxhor23} >                         
                <IconD name="chevron-right" size={80}  style={styles.ikon2}/>
              </View>            
            </ScrollView>             
            </View>     
            
            <View style={styles.Box2}>
            <ScrollView horizontal >
              <View style={styles.boxhor21} >                         
                <IconD name="git" size={80}  style={styles.ikon}/>
              </View>
              <View style={styles.boxhor22} >                         
                  <Text style={styles.judul4}>Git</Text>
                  <Text style={styles.jenis2}>Teknologi</Text>                  
                  <Text style={styles.persen}>75%</Text> 
              </View>
              <View style={styles.boxhor23} >                         
                <IconD name="chevron-right" size={80}  style={styles.ikon2}/>
              </View>            
            </ScrollView> 
            </View>       
            <View style={styles.Box2}>
            <ScrollView horizontal >
              <View style={styles.boxhor21} >                         
                <IconD name="gitlab" size={80}  style={styles.ikon}/>
              </View>
              <View style={styles.boxhor22} >                         
                  <Text style={styles.judul4}>Gitlab</Text>
                  <Text style={styles.jenis2}>Teknologi</Text>                  
                  <Text style={styles.persen}>60%</Text> 
              </View>
              <View style={styles.boxhor23} >                         
                <IconD name="chevron-right" size={80}  style={styles.ikon2}/>
              </View>            
            </ScrollView> 
            </View>                 
        </View>        
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  layout: {
    flex: 1
  },
  logo: {       
    width: 200,
    height: 45,
  },
  logojudul: {    
    alignItems:  'flex-end'
  },
  logoitem: {
    color: '#3EC6FF',
    fontSize: 12, 
    paddingRight :30
  },
  judul:{
    alignItems: 'flex-start',
  },
  body:{
    alignItems: 'flex-start',
    paddingLeft :5,
    flex: 1
  },
  Box:{
    width: 350, 
    margin: 5,
  }, 
  Box1:{
    marginLeft: 12,
  }, 
  Box11:{    
    borderRadius:20,
    backgroundColor:'#003366',
    width: 100,  
    height: 150,
    margin: 5,
  },
  Box2:{  
    marginTop:10,
    borderRadius:20,
    backgroundColor:'#B4E9FF',
    width: 350,  
    height: 130,
    margin: 5,  
    alignItems: 'flex-start',    
    paddingTop:10, 
    paddingBottom: 10,
    paddingRight: 20,     
    paddingLeft:15,  
  },
  
Boxhor21:{
  alignItems: 'center',
  flexDirection : 'row',
},
Boxhor22:{
  flexDirection: 'column'
},
Boxhor23:{
  alignSelf: 'flex-end',
  flexDirection : 'row',
},
  judul1:{
    color: '#003366',
    fontSize: 12,
    fontWeight: 'bold'
  },
  judul2:{
    color: '#003366',
    fontSize: 16,
    fontWeight: 'bold'
  },
  judul3:{
    color: '#003366',
    fontSize: 36,
    
  marginLeft: 15, 
  }  ,
  judul4:{
    color: '#003366',
    fontSize: 18,
    textAlign:'left',    
    fontWeight: 'bold'  ,
    paddingLeft :24,
  },
  isi:{
    color: '#003366',
    fontSize: 13,
    textAlign:'center',
    marginLeft :5,  
  },
  
  jenis:{
    color: '#3EC6FF',
    fontSize: 13,
    textAlign:'left',
    marginLeft:20
  },
   
  jenis2:{
    color: '#3EC6FF',
    fontSize: 13,
    textAlign:'left',
    marginLeft:80
  },
  persen:{
    color: '#FFFFFF',
    fontSize: 48,
    textAlign:'left',
    alignSelf:'flex-end'
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'black',
    margin:10,
},
lineStyle2:{
  borderWidth: 3,
  Width : 450,
  borderColor:'#3EC6FF',
  margin:10,
  alignItems: 'strech'
}
,
ikon:{
  alignSelf: 'center',
  color : '#003366' ,  
  marginTop:12
},
ikon1:{  
  color:'#FFFFFF',
  backgroundColor:'#3EC6FF',
  width: 30,
  height: 30,
  paddingLeft :7,
  borderRadius: 70/2,
  
},
ikon2:{
  alignSelf: 'center',
  color : '#003366', 
  marginLeft:50,
  marginTop:12
},
boxSmall: {
  paddingLeft :3,
  paddingRight :7,
  height: 20,
  //marginBottom: 10,
  marginRight:3,
  alignItems: 'center',  
  backgroundColor:'#3EC6FF',  
  borderRadius: 70/2,
},

boxHor1: {
  paddingLeft :5,
  width: 30,
  height: 30,
  marginLeft: 15, 
  marginBottom: 10,
  alignItems: 'flex-start'
},
boxHor2: {
  paddingLeft :5,
  width: 200,
  height: 30,
  marginLeft :5, 
  marginBottom: 10,
  
},
})