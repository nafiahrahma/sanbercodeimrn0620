import React, { Component } from 'react'
import { View,
   StyleSheet,
  Text,
  Image,
  Button,
  TextInput 
  } from 'react-native'

//import Toggle from './Latihan/Toggle'
export default class LoginScreen extends Component {

  state = {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'strech',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    return (
      <View style={styles.container}>        
        <View style={[styles.layout, layoutStyle]}>
          <View style={styles.logojudul} >              
                <Image style={styles.logo} source={require('./images/logo.png')} />  
                <Text style={styles.logoitem}>PORTOFOLIO</Text>
              
          </View>
          <View style={styles.body}>
            <Text style={styles.judul}>Login</Text>  
          </View> 
          <View style={styles.body}>   
            <Text style={styles.fieldjudul}>Username/Email</Text>
            <View style={styles.box} >                  
              <TextInput  style={styles.box}    />
              <Text>{this.state.value}</Text>          
            </View>            
            <Text style={styles.fieldjudul}>Password</Text>           
            <View style={styles.box} >                  
              <TextInput  style={styles.box}    />
              <Text>{this.state.value}</Text>          
            </View>  
          </View> 
            
            <View style={styles.bodyisi}> 
              <View style={styles.myButton1} >                  
              <Button                  
                  title="Masuk"  
                 color="#3EC6FF"
                />         
              </View>  
            <View style={styles.bodyisi}>          
            <Text style={styles.atau}>atau</Text>
            </View>           
            <View style={styles.myButton2} >                  
              <Button                  
                  title="Daftar ?"  
                 color="#003366"
                />         
              </View> 
              
            
       
          </View>
          <View style={styles.foot} >                  
                   
                   </View>
          
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  layout: {
    flex: 1
  },
  box: {    
    height: 40, 
    width: 300,
    borderWidth: 1 ,
  },
  logo: {       
    alignItems: 'center',  
    width: 375,
    height: 102 ,
    resizeMode: 'stretch', 
  },
  logojudul: {    
    alignItems:  'flex-end'
  },
  logoitem: {
    color: '#3EC6FF',
    fontSize: 24, 
    paddingRight :30
  },
  judul: {
    color: '#003366',
    fontSize: 24,
    alignSelf: 'center'
    
    
  },
  body:{
    alignItems: 'flex-start',
    paddingLeft :30
  },
  bodyisi:{
    alignSelf: 'center',
    
  },
  atau:{
    color: '#3EC6FF',
    fontSize: 24, 
  },
 fieldjudul: {    
    color: '#003366',
    fontSize: 16,  
    
  },
  fieldbox: {    
    height: 50,
    marginBottom: 10,
    marginRight: 10
  },
  foot: {    
    height: 80, 
    width: 300,
  },
  myButton1:{
    padding: 5,
    borderRadius:20,
    backgroundColor:'#3EC6FF',
    width: 150,
  },
  myButton2:{
    padding: 5,
    borderRadius:20,
    backgroundColor:'#003366',
    width: 150,
  }
})