import React, { Component } from 'react'
import { View,
   StyleSheet,
  Text,
  ScrollView
  } from 'react-native'

//import Toggle from './Latihan/Toggle'
import IconD from 'react-native-vector-icons/AntDesign';

import Icon from 'react-native-vector-icons/Ionicons';
export default class AboutScreen extends Component {

  state = {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'strech',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    return (
      <View style={styles.container}>        
        <View style={[styles.layout, layoutStyle]}>
          <View style={styles.judul} >               
              <Text style={styles.judul1}>Tentang Saya</Text>
             
                <Icon name="md-person"  size={170} style={styles.ikon1}/>
                <Text style={styles.judul2}>Rahmawati Nafiah</Text>
                <Text style={styles.judul3}>React Native Developer</Text>
          </View>
          <View style={styles.body}>
            <View style={styles.Box1}>
            <Text style={styles.judul4}>Portofolio</Text>
            <View style = {styles.lineStyle} />
            <ScrollView horizontal>
              <View style={styles.boxSmall} color='#3EC6FF' >
                  <IconD name="gitlab" size={50} color='#3EC6FF'/>
                <Text style={styles.isi} >@nafiahrahma</Text>
              </View>
              <View style={styles.boxSmall} >                
                <Icon name="logo-github" size={50} color='#3EC6FF'/>          
                <Text style={styles.isi} >@nafiahrahma</Text>
              </View>
            </ScrollView>         
            </View>           
            <View style={styles.Box2}>
                <Text style={styles.judul4}>Hubungi Saya</Text>                
                <View style = {styles.lineStyle} />
               
                  <View style={styles.Box2hor}>
                      <Icon name="logo-facebook" size={50}  style={styles.ikon}/>
                      <Text style={styles.isi}>rahamwatinafiah</Text>
                    </View>
                    <View style={styles.Box2hor}>
                      <Icon name="logo-instagram" size={50} style={styles.ikon}/>           
                      <Text style={styles.isi}>@nafiahrahma</Text> 
                    </View>
                    <View style={styles.Box2hor}>
                      <Icon name="logo-twitter" size={50} style={styles.ikon}/>           
                      <Text style={styles.isi}>@nafiahrahma</Text>
                    </View>   
                
            </View>
          </View> 
         
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  layout: {
    flex: 1
  },
  judul:{
    alignItems: 'center',
    paddingTop: 50

  },
  body:{
    alignItems: 'flex-start',
    paddingLeft :5,
    flex: 1
  },
  Box1:{
    borderRadius:20,
    backgroundColor:'#EFEFEF',
    width: 350,  
    height: 150,
    margin: 5,
  }, 
  Box2:{  
    borderRadius:20,
    backgroundColor:'#EFEFEF',
    width: 350,  
    height: 250,
    margin: 5,    
  },
  judul1:{
    color: '#003366',
    fontSize: 36,
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  judul2:{
    color: '#003366',
    fontSize: 24,
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  judul3:{
    color: '#3EC6FF',
    fontSize: 16,
    alignSelf: 'center',
  }  ,
  judul4:{
    color: '#003366',
    fontSize: 18,
    textAlign:'left',
    paddingLeft :15,
    paddingTop: 5
  },
  isi:{
    color: '#003366',
    fontSize: 16,
    textAlign:'center',
    fontWeight: 'bold'  ,
    marginLeft :5,  
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'black',
    margin:10,
}
,
ikon:{
  alignSelf: 'center',
  color : '#3EC6FF' 
},
ikon1:{
  alignItems: 'center',
  color:'grey',
  backgroundColor:'#EFEFEF',
  width: 200,
  height: 200,
  paddingLeft :35,
  borderRadius: 200/2,
},
boxSmall: {
  paddingLeft :30,
  width: 160,
  height: 50,
  marginBottom: 10,
  marginRight: 10,
  alignItems: 'center'
},
Box2hor:{
  flexDirection: 'row',
  alignItems: 'center',
  marginLeft :100,
  
  marginBottom: 10,
  marginRight: 10,
}
})