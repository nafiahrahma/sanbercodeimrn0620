import React from "react";

import{NavigationContainer} from'@react-navigation/native';
import{createStackNavigator} from '@react-navigation/stack';
import{createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import{createDrawerNavigator}from "@react-navigation/drawer";
import{SignIn,CreateAccount, Search,Login, Details,Search2,
  Skill, Add,Project, Profile} from './Screen';

const AuthStack=createStackNavigator();
const Tabs =createBottomTabNavigator();
const LoginStack=createStackNavigator();
const SearchStack=createStackNavigator();

const LoginStackScreen=()=>(
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login}/>
    <LoginStack.Screen 
      name="Details" 
      component={Details} 
      options={({route})=>({
      title: route.params.name
    })}/>
  </LoginStack.Navigator>
)

const ProfileStack=createStackNavigator();
const ProfileStackScreen=()=>(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile}/>
  </ProfileStack.Navigator>
)

const SearchStackScreen=()=>(
  <SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search}/>
    <SearchStack.Screen name="Search2" component={Search2}/>
  </SearchStack.Navigator>
)
const TabsScreen=()=>(
  <Tabs.Navigator>
      <Tabs.Screen name="Skill" component={Skill}/>
      <Tabs.Screen name="Project" component={Project}/>      
      <Tabs.Screen name="Add" component={Add}/>
    </Tabs.Navigator>
);

const Drawer =createDrawerNavigator();
const DrawerScreen=()=>(
<Drawer.Navigator>
      <Drawer.Screen name="Login" component={TabsScreen}/>
      <Drawer.Screen name="Profile" component={ProfileStackScreen}/>
    </Drawer.Navigator>
);

export default()=>(
  <NavigationContainer>    
    {<AuthStack.Navigator>
      <AuthStack.Screen 
      name="SignIn" 
      component={SignIn} 
      options={{title:'Sign in'}} />
      <AuthStack.Screen 
      name="CreateAccount" 
      component={CreateAccount} 
      options={{title:'Create Account' }}/>
    </AuthStack.Navigator> }
  </NavigationContainer>
)




/* 

const RootStack=createStackNavigator();
const Drawer =createDrawerNavigator();
const Tabs=createBottomTabNavigator();



const TabsScreen=()=>(
  <Tabs.Navigator>
      <Tabs.Screen name="Skill" component={SkillScreen}/>
      <Tabs.Screen name="Project" component={ProjectScreen}/>      
      <Tabs.Screen name="Add" component={AddScreen}/>
    </Tabs.Navigator>
);

const DrawScreen=()=>(
  <Drawer.Navigator>
      <Drawer.Screen name="Tabs" component={TabsScreen}/>
      <Drawer.Screen name="About" component={AboutScreen}/>
    </Drawer.Navigator>
);



export default()=>(
  <NavigationContainer>
      <RootStack.Navigator>
        <RootStack.Screen name="Login" component={Login}/>
        <RootStack.Screen name="Drawer" component={DrawScreen}/>
      </RootStack.Navigator>  
  </NavigationContainer>
)



 */